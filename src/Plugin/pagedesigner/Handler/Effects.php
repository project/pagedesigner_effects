<?php

namespace Drupal\pagedesigner_effects\Plugin\pagedesigner\Handler;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Serialization\Yaml as YamlSerializer;
use Drupal\pagedesigner\Entity\Element;
use Drupal\pagedesigner\Plugin\HandlerConfigTrait;
use Drupal\pagedesigner\Plugin\HandlerPluginBase;
use Drupal\pagedesigner\Plugin\HandlerUserTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml as YamlParser;

/**
 * Add effects functionality to "row" and "component" patterns.
 *
 * @PagedesignerHandler(
 *   id = "effects",
 *   name = @Translation("Effects handler"),
 *   types = {
 *      "row",
 *      "component"
 *   },
 *   weight = 50
 * )
 */
class Effects extends HandlerPluginBase {

  // Import config property and setter.
  use HandlerConfigTrait;
  // Import user property and setter.
  use HandlerUserTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->setConfigFactory($container->get('config.factory'));
    $instance->setCurrentUser($container->get('current_user'));
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function collectAttachments(array &$attachments) {
    if ($this->currentUser->hasPermission('pd_effect_use')) {
      $config = $this->configFactory->get('pagedesigner_effects.settings');
      $attachments['drupalSettings']['pagedesigner_effects']['effects'] = YamlParser::parse(YamlSerializer::decode($config->get('enable_effects')));
      $attachments['library'][] = 'pagedesigner_effects/pagedesigner';
    }
  }

  /**
   * {@inheritDoc}
   */
  public function serialize(Element $entity, array &$result = []) {
    if (!$entity->field_effects->isEmpty()) {
      $result['effects'] = Json::decode((string) $entity->field_effects->value);
    }
    else {
      $result['effects'] = [];
    }
  }

  /**
   * {@inheritDoc}
   */
  public function patch(Element $entity, array $data) {
    if ($entity->hasField('field_effects')) {
      if (!empty($data['effects'])) {
        $entity->field_effects->value = Json::encode($data['effects']);
      }
      else {
        $entity->field_effects->value = '';
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function view(Element $entity, string $view_mode, array &$build = []) {

  }

  /**
   * {@inheritDoc}
   */
  public function build(Element $entity, string $view_mode, array &$build = []) {
    $this->addEffects($entity, $build);
  }

  /**
   * Add the user generated effects of the entity to drupalSettings.
   *
   * @param \Drupal\pagedesigner\Entity\Element $entity
   *   The entity being rendered.
   * @param array $build
   *   The render array.
   */
  protected function addEffects(Element $entity, array &$build = []) {
    if ($entity->hasField('field_effects') && !$entity->field_effects->isEmpty()) {
      if (empty($build['#attached'])) {
        $build['#attached'] = [];
      }
      if (empty($build['#attached']['drupalSettings'])) {
        $build['#attached']['drupalSettings'] = [];
      }
      if (empty($build['#attached']['drupalSettings']['pagedesigner'])) {
        $build['#attached']['drupalSettings']['pagedesigner'] = [];
      }
      if (empty($build['#attached']['drupalSettings']['pagedesigner']['effects'])) {
        $build['#attached']['drupalSettings']['pagedesigner']['effects'] = [];
      }
      $build['#attached']['drupalSettings']['pagedesigner']['effects']['#pd-cp-' . $entity->id()] = Json::decode((string) $entity->field_effects->value);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function get(Element $entity, string &$result = '') {
  }

  /**
   * {@inheritdoc}
   */
  public function getContent(Element $entity, array &$list = [], $published = TRUE) {
  }

  /**
   * {@inheritdoc}
   */
  public function delete(Element $entity, bool $remove = FALSE) {
  }

  /**
   * {@inheritdoc}
   */
  public function restore(Element $entity) {}

  /**
   * {@inheritDoc}
   */
  public function generate($definition, array $data, Element &$entity = NULL) {
  }

  /**
   * {@inheritdoc}
   */
  public function copy(Element $entity, Element $container = NULL, Element &$clone = NULL) {
  }

  /**
   * {@inheritdoc}
   */
  public function publish(Element $entity) {
  }

  /**
   * {@inheritdoc}
   */
  public function unpublish(Element $entity) {
  }

}
